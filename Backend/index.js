const APP = require('restify');
const POST = 3001;

const server = APP.createServer({
	name: 'simple-e-commerce',
	version: '0.1',
});

/**
  * Start Server, Connect to DB & Require Routes
  */
server.listen(POST, () => {
    console.log(`Server is listening on port ${POST}`);
});